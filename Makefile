TARGET = potoki
CC = gcc
CFLAGS = -Wall -g

all: $(TARGET).c
	$(CC) $(CFLAGS) -o $(TARGET) $(TARGET).c
test:
	./$(TARGET)
clean: 
	$(RM) $(TARGET)
